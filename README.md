SHElock escape room GUI
=======================


This is the GUI which is used in the SHElock escape room, a neuroscience
themed escape room concept.


The GUI is a Python application which is based upon wxPython.


Dependencies
------------


All dependencies are listed in ``requirements.txt``.


Usage
-----


The application entry point is the ``main`` function in
``escaperoom/main.py``. You can run directly from the code like so:


```
python -m escaperoom
```


Standalone macOS builds
-----------------------


``setup.py`` contains logic to generate a stand-alone version of the GUI using
``py2app``. You can generate a build like so:


```
python setup.py standalone
```