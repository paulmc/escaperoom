#!/usr/bin/env python


import os.path as op
import time

import numpy as np
import wx

import fsl.data.image as fslimage
import fsleyes_widgets.utils.progress as progress
import fsleyes_widgets.floatspin as floatspin
import fsleyes.views.orthopanel as ortho

from . import helppanel


class ContrastPanel(helppanel.HelpPanel):

    def __init__(self, mainFrame, dataDir, stage, **kwargs):
        helppanel.HelpPanel.__init__(
            self, mainFrame, dataDir, 'contrasts.mp4', **kwargs)

        self.__mainFrame = mainFrame
        self.__dataDir   = dataDir
        self.__stage     = stage

        self.__modalities = []
        self.__params     = {}
        self.__paramVals  = {}
        with open(op.join(dataDir, 'contrasts', 'modalities.txt')) as f:
            for line in f:

                line = line.strip()

                if line == '' or line.startswith('#'):
                    continue

                modality, params = line.split(maxsplit=1)
                params           = params.split(',')
                params, pvals    = zip(*[p.split(':') for p in params])
                params           = [p.strip()        for p in params]
                pvals            = [float(p.strip()) for p in pvals]

                self.__modalities.append(modality)
                self.__params[modality] = params
                for param, pval in zip(params, pvals):
                    self.__paramVals[modality, param] = pval

        self.__modality        = wx.StaticText(
            self, label='Modality: {}'.format(self.__modalities[0]))
        self.__scanButton      = wx.Button(self, label='Scan')
        self.__identifyButton  = wx.Button(self, label='Locate abnormality')
        self.__paramPanel      = wx.Panel(self)
        self.__location        = wx.StaticText(self, label='Location: ???')
        self.__paramWidgets    = {}

        lfont  = wx.Font(wx.FontInfo()
                         .FaceName('Courier')
                         .Bold())
        lfont.SetPointSize(18)
        self.__location.SetFont(lfont)

        # gets replaced in the __onScan method
        self.__orthoPanel = wx.Panel(self)
        self.__orthoPanel.SetBackgroundColour('#000000')

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add(self.__scanButton,     flag=wx.EXPAND, proportion=1)
        btnSizer.Add(self.__identifyButton, flag=wx.EXPAND, proportion=1)

        self.mainSizer.Add(self.__modality,       flag=wx.EXPAND)
        self.mainSizer.Add(self.__paramPanel,     flag=wx.EXPAND)
        self.mainSizer.Add(btnSizer,              flag=wx.EXPAND)
        self.mainSizer.Add(self.__orthoPanel,     flag=wx.EXPAND, proportion=1)
        self.mainSizer.Add(self.__location,       flag=wx.EXPAND)

        self.__scanButton    .Bind(wx.EVT_BUTTON, self.__onScan)
        self.__identifyButton.Bind(wx.EVT_BUTTON, self.__onIdentify)

        self.__mainFrame.displayCtx.addListener(
            'location', 'contrastpanel', self.__locationChanged)
        self.__modalityChanged()


    def __modalityChanged(self, ev=None):

        modality = self.__modalities[0]
        params   = self.__params[modality]
        panel    = self.__paramPanel
        pwidgets = {}
        sizer    = wx.FlexGridSizer(len(params), 2, 0, 0)
        sizer.AddGrowableCol(1)

        panel.DestroyChildren()
        panel.SetSizer(sizer)

        for p in params:

            label = wx.StaticText(self.__paramPanel, label=p)
            entry = wx.TextCtrl(self.__paramPanel)
            sizer.Add(label, flag=wx.EXPAND)
            sizer.Add(entry, flag=wx.EXPAND, proportion=1)
            pwidgets[p] = entry

        panel.Layout()
        self.Layout()
        self.__paramWidgets = pwidgets


    def __onIdentify(self, ev):
        dlg = IdentifyDialog(self)
        dlg.CentreOnParent()
        dlg.ShowModal()
        exp    = np.loadtxt(op.join(self.__dataDir,
                                    'contrasts',
                                    'coordinates.txt'))
        got    = dlg.GetCoordinates()
        result = np.all(exp == got)

        title = 'MRI diagnosis'

        if result:
            msg = 'Abnormality detected at coordinates ' \
                  '[{} {} {}] - positive result'.format(*got)
            icon = wx.ICON_INFORMATION
        else:
            msg = 'No abnormalities found - negative result'
            icon = wx.ICON_ERROR

        wx.MessageDialog(
            self,
            message=msg,
            caption=title,
            style=wx.OK | icon).ShowModal()

        if result:
            self.__mainFrame.completed(self.__stage)


    def __onScan(self, ev):
        modality  = self.__modalities[0]
        params    = self.__params[modality]
        pwidgets  = self.__paramWidgets
        paramVals = {p : pwidgets[p].GetValue()        for p in params}
        goodVals  = {p : self.__paramVals[modality, p] for p in params}
        title     = 'Scanning'
        msg       = 'Acquiring {} scan ...'.format(modality)
        dlg       = progress.Bounce(title, msg, values=range(20))

        for k, v in paramVals.items():
            try:               paramVals[k] = float(v)
            except ValueError: pass

        for i in range(20):
            dlg.Update((i + 1) * 5)
            time.sleep(0.1)
            wx.Yield()

        goodScan = all([paramVals[p] == goodVals[p] for p in params])

        if goodScan: imgname = '{}_good'.format(modality)
        else:        imgname = '{}_bad' .format(modality)

        imgfile = op.join(self.__dataDir, 'contrasts', imgname)
        img     = fslimage.Image(imgfile)

        self.__mainFrame.overlayList[:] = [img]

        display = self.__mainFrame.displayCtx.getDisplay(img)
        opts    = display.opts

        opts.interpolation = 'spline'

        if goodScan: opts.displayRange =     0, 500
        else:        opts.displayRange = -500, 1000

        if not isinstance(self.__orthoPanel, ortho.OrthoPanel):
            self.mainSizer.Detach(self.__orthoPanel)
            self.__orthoPanel.Destroy()

            self.__orthoPanel = ortho.OrthoPanel(
                self,
                self.__mainFrame.overlayList,
                self.__mainFrame.displayCtx,
                self.__mainFrame)

            self.mainSizer.Insert(
                3, self.__orthoPanel, flag=wx.EXPAND, proportion=1)
            self.Layout()

        self.__orthoPanel.getCurrentProfile().centreCursor()


    def __locationChanged(self, *a):

        dctx = self.__mainFrame.displayCtx
        opts = dctx.getOpts(dctx.getSelectedOverlay())
        vox  = opts.getVoxel()
        if vox is None:
            return
        vox  = np.floor(np.array(vox) / 20)
        self.__location.SetLabel('Location: {} {} {}'.format(*vox))


class IdentifyDialog(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, style=wx.DEFAULT_DIALOG_STYLE)

        self.numsizer = wx.FlexGridSizer(3, 2, 0, 0)
        self.numsizer.AddGrowableCol(1)

        self.mainsizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.mainsizer)

        self.label  = wx.StaticText(self, label='Enter abnormality XYZ coordinates')
        self.xlabel = wx.StaticText(self, label='X')
        self.ylabel = wx.StaticText(self, label='Y')
        self.zlabel = wx.StaticText(self, label='Z')
        self.xentry = floatspin.FloatSpinCtrl(self)
        self.yentry = floatspin.FloatSpinCtrl(self)
        self.zentry = floatspin.FloatSpinCtrl(self)
        self.button = wx.Button(self, label='Ok')

        self.numsizer .Add(self.xlabel, flag=wx.EXPAND)
        self.numsizer .Add(self.xentry, flag=wx.EXPAND)
        self.numsizer .Add(self.ylabel, flag=wx.EXPAND)
        self.numsizer .Add(self.yentry, flag=wx.EXPAND)
        self.numsizer .Add(self.zlabel, flag=wx.EXPAND)
        self.numsizer .Add(self.zentry, flag=wx.EXPAND)

        self.mainsizer.Add((5, 5),        flag=wx.EXPAND)
        self.mainsizer.Add(self.label,    flag=wx.EXPAND | wx.ALL, border=5)
        self.mainsizer.Add((5, 5),        flag=wx.EXPAND)
        self.mainsizer.Add(self.numsizer, flag=wx.EXPAND | wx.ALL, border=5,
                           proportion=1)
        self.mainsizer.Add((5, 5),        flag=wx.EXPAND)
        self.mainsizer.Add(self.button,   flag=wx.EXPAND)

        self.button.Bind(wx.EVT_BUTTON, self.__onOk)


    def __onOk(self, ev):
        self.EndModal(wx.ID_OK)


    def GetCoordinates(self):
        return (self.xentry.GetValue(),
                self.yentry.GetValue(),
                self.zentry.GetValue())
