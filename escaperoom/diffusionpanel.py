#!/usr/bin/env python


from . import helppanel


class DiffusionPanel(helppanel.HelpPanel):

    def __init__(self, mainFrame, dataDir, stage):
        helppanel.HelpPanel.__init__(self, mainFrame, dataDir, 'diffusion.mov')
