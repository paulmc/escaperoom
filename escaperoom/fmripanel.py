#!/usr/bin/env python
#
# fmripanel.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os.path as op
import            glob
from collections import OrderedDict

import wx
import wx.lib.newevent as wxevent
import numpy as np
import matplotlib as mpl

mpl.use('WxAgg')

import matplotlib.pyplot as plt
import matplotlib.image  as mpimg
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as Canvas

import fsl.utils.idle as idle

from . import stimpanel
from . import helppanel


def calc_extent(bmp, xlen, xcentre, ycentre, dxlen, dylen, canvas):

    cwidth, cheight = canvas.get_width_height()

    brat = bmp.shape[0] / bmp.shape[1]
    crat = cwidth / cheight
    drat = dxlen / dylen

    ylen = xlen / drat * crat * brat

    return [xcentre - 0.5 * xlen,
            xcentre + 0.5 * xlen,
            ycentre - 0.5 * ylen,
            ycentre + 0.5 * ylen]


class FMRIPanel(helppanel.HelpPanel):

    def __init__(self, mainFrame, dataDir, stage, **kwargs):
        helppanel.HelpPanel.__init__(self, mainFrame, dataDir, 'fmri.mp4', **kwargs)

        self.__mainFrame    = mainFrame
        self.__dataDir      = dataDir
        self.__stage        = stage
        self.__sidePanel    = wx.Panel(self)
        self.__stimPanel    = stimpanel.StimulusPanel(self.__sidePanel, dataDir)
        self.__resetButton  = wx.Button(self.__sidePanel, label='Reset')
        self.__runButton    = wx.Button(self.__sidePanel, label='Run experiment')
        self.__finishPanel  = wx.Panel( self.__sidePanel)
        self.__finishButton = wx.Button(self.__finishPanel,
                                        label='Finish experiment')

        self.__resetButton.Disable()

        self.__figure = plt.Figure()
        self.__axis   = self.__figure.add_subplot(111)
        self.__canvas = Canvas(self, -1, self.__figure)

        self.__axis.spines['right'] .set_visible(False)
        self.__axis.spines['left']  .set_visible(False)
        self.__axis.spines['top']   .set_visible(False)
        self.__axis.spines['bottom'].set_visible(False)

        self.__figure.patch.set_color('#ffffff')
        self.__figure.subplots_adjust(top=1.0, bottom=0.1, left=0.0, right=1.0)

        self.__activeStim = []

        self.__finishSizer = wx.BoxSizer(wx.VERTICAL)
        self.__finishSizer.Add(self.__finishButton,
                               flag=wx.EXPAND | wx.ALL,
                               border=5)
        self.__finishPanel.SetSizer(self.__finishSizer)

        self.__sideSizer = wx.BoxSizer(wx.VERTICAL)
        self.__sidePanel.SetSizer(self.__sideSizer)
        self.__sideSizer.Add((1, 1),              flag=wx.EXPAND, proportion=1)
        self.__sideSizer.Add(self.__runButton,    flag=wx.EXPAND)
        self.__sideSizer.Add((10, 5),             flag=wx.EXPAND)
        self.__sideSizer.Add(self.__resetButton,  flag=wx.EXPAND)
        self.__sideSizer.Add((10, 5),             flag=wx.EXPAND)
        self.__sideSizer.Add(self.__finishPanel,  flag=wx.EXPAND)
        self.__sideSizer.Add((10, 5),             flag=wx.EXPAND)
        self.__sideSizer.Add(self.__stimPanel,    flag=wx.EXPAND)
        self.__sideSizer.Add((1, 1),              flag=wx.EXPAND, proportion=1)

        self.__sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.__sizer.Add((5, 5),           flag=wx.EXPAND)
        self.__sizer.Add(self.__sidePanel, flag=wx.EXPAND)
        self.__sizer.Add((5, 5),           flag=wx.EXPAND)
        self.__sizer.Add(self.__canvas,    flag=wx.EXPAND, proportion=1)

        self.mainSizer.Add(self.__sizer, flag=wx.EXPAND, proportion=1)

        self.__stimPanel.Bind(stimpanel.EVT_STIM_ON,  self.__stimulusOn)
        self.__stimPanel.Bind(stimpanel.EVT_STIM_OFF, self.__stimulusOff)

        self.__runButton.Bind(wx.EVT_BUTTON, self.run)
        self.__resetButton.Bind(wx.EVT_BUTTON, self.__reset)
        self.__finishButton.Bind(wx.EVT_BUTTON, self.__finish)

        self.__finishButton.Disable()

        self.__finishedExperiments = set()
        self.__stimuli = OrderedDict()
        self.__correct = {}
        self.__stimlevels = {}
        self.__baselineStim = None
        self.__finished = False

        with open(op.join(dataDir, 'fmri', 'stimuli.txt'), 'rt') as f:
            for line in f:
                order, name, correct = line.strip().split()
                correct = correct == 'true'

                self.__stimuli[order] = name
                if correct:
                    self.__correct[order] = name

        with open(op.join(dataDir, 'fmri', 'stimuli', 'levels.txt'), 'rt') as f:
            for line in f:
                line = line.strip()
                stim, level = line.split()
                level = float(level)
                self.__stimlevels[stim] = level
                if level == 1:
                    self.__baselineStim = stim

        maxlevel = max(self.__stimlevels.values())
        minlevel = min(self.__stimlevels.values())
        for s, l in self.__stimlevels.items():
            self.__stimlevels[s] = (l - minlevel) / (maxlevel - minlevel)

        self.__stimimgs = {}
        bmps = glob.glob(op.join(dataDir, 'fmri', 'stimuli', '*.png'))
        for b in bmps:
            name = op.splitext(op.basename(b))[0]
            self.__stimimgs[name] = mpimg.imread(b)

        self.__activations = {}
        bmps = glob.glob(op.join(dataDir, 'fmri', 'activations', '*.png'))
        for b in bmps:
            name = op.splitext(op.basename(b))[0]
            self.__activations[name] = mpimg.imread(b)

        self.__contrasts = {}
        bmps = glob.glob(op.join(dataDir, 'fmri', 'contrasts', '*.png'))
        for b in bmps:
            name = op.splitext(op.basename(b))[0]
            stim1, stim2 = name.split('_')
            self.__contrasts[stim1, stim2] = mpimg.imread(b)

        self.__contrastLabels = {}
        txts = glob.glob(op.join(dataDir, 'fmri', 'labels', '*.txt'))
        for t in txts:
            name = op.splitext(op.basename(t))[0]
            stim1, stim2 = name.split('_')
            with open(t, 'rt') as f:
                lines = f.read().strip().split('\n')
                lines = [l.strip() for l in lines]
                lines = [l for l in lines if l != '']
                self.__contrastLabels[stim1, stim2] = lines

        self.__drawPlot()

        self.__stimulusPatch     = None
        self.__timePatch         = None
        self.__finalPatches      = None


    def onshow(self):

        helppanel.HelpPanel.onshow(self)

        options = list(self.__stimuli.values())
        correct = list(self.__correct.values())

        dlg = StimSelectDialog(self, options, correct)
        dlg.CentreOnParent()
        dlg.ShowModal()

        for sid in self.__stimuli.keys():
            self.__stimPanel.EnableButton(sid, sid in self.__correct)


    def __finish(self, ev):
        self.__finished = True
        self.__mainFrame.completed(self.__stage)


    def __initMovie(self):

        stim = self.__activeStim
        if len(stim) == 0:
            return

        self.__timePatch = self.__axis.axvline(0, c='#000080', lw=2)

        bmp = self.__stimimgs[stim[0]]
        extent = calc_extent(
            bmp, 0.3, 0.5, 4.0, 1.2, 5.5, self.__canvas)

        self.__stimulusPatch = self.__axis.imshow(
            bmp,
            extent=extent,
            interpolation='bilinear',
            aspect='auto')


    def __movieFrame(self, time):

        idx = int(np.floor(time * len(self.__activeStim)))

        if idx >= len(self.__activeStim):
            return

        stim = self.__activeStim[idx]
        bmp  = self.__stimimgs[stim]
        extent = calc_extent(
            bmp, 0.3, 0.5, 4.0, 1.2, 5.5, self.__canvas)

        self.__timePatch.set_xdata(time)
        self.__stimulusPatch.set_data(bmp)
        self.__stimulusPatch.set_extent(extent)
        self.__canvas.draw()


    def __endMovie(self):
        self.__stimulusPatch.remove()
        self.__timePatch.remove()

        self.__stimulusPatch = None
        self.__timePatch = None
        self.__canvas.draw()

        stim = sorted(self.__activeStim)
        if len(stim) == 2 and stim[0] == '01' and stim[1] in self.__correct.keys():
            self.__finishedExperiments.add(stim[1])

        if all([k in self.__finishedExperiments for k in self.__correct.keys()]):
            self.__finishButton.Enable()
            self.__flashFinish()


    def __flashFinish(self):

        if self.__finished:
            return

        colour = self.__finishPanel.GetBackgroundColour()
        if colour == '#ff8888': colour = '#ffffff'
        else:                   colour = '#ff8888'
        self.__finishPanel.SetBackgroundColour(colour)
        self.__finishPanel.Refresh()
        idle.idle(self.__flashFinish, after=1.0)


    def __finalMovieFrameOneStimulus(self):
        stim = self.__activeStim[0]
        bmp = self.__activations[stim]

        self.__finalPatches = []

        extent = calc_extent(
            bmp, 0.18, 0.5, 4.0, 1.2, 5.5, self.__canvas)

        self.__finalPatches.append(self.__axis.text(
            0.5, 4.85,
            'Brain activity in response to the stimulus:',
            ha='center'))

        self.__finalPatches.append(self.__axis.imshow(
            bmp,
            extent=extent,
            interpolation='bilinear',
            aspect='auto'))

        haveBaseline = self.__baselineStim in self.__activeStim

        if haveBaseline: missing = 'experimental'
        else:            missing = 'baseline'

        msg = 'No {} stimulus was included in the experiment -\n' \
            'overall brain activity cannot be calculated!'.format(missing)

        self.__finalPatches.append(self.__axis.text(
            0.5, 2.95,
            msg,
            ha='center'))

        self.__canvas.draw()


    def __finalMovieFrameTwoStimuli(self):
        stim    = self.__activeStim
        actbmps = [self.__activations[s] for s in stim]
        cwidth, cheight = map(float, self.__canvas.get_width_height())

        self.__finalPatches = []

        self.__finalPatches.append(self.__axis.text(
            0.5, 4.85,
            'Brain activity in response to the stimuli:',
            ha='center'))

        haveBaseline = self.__baselineStim in self.__activeStim

        for i, bmp in enumerate(actbmps):

            extent = calc_extent(
                bmp, 0.18, 0.2 + 0.6 * i, 4.0, 1.2, 5.5, self.__canvas)

            self.__finalPatches.append(self.__axis.imshow(
                bmp,
                extent=extent,
                interpolation='bilinear',
                aspect='auto'))

        if not haveBaseline:
            pass

        self.__finalPatches.append(self.__axis.text(
            0.5, 2.95,
            'Contrast between the two stimuli:',
            ha='center'))

        stim1, stim2 = sorted(stim)
        bmp          = self.__contrasts[stim1, stim2]
        extent = calc_extent(bmp, 0.25, 0.5, 1.9, 1.2, 5.5, self.__canvas)

        self.__finalPatches.append(self.__axis.imshow(
            bmp,
            extent=extent,
            interpolation='bilinear',
            aspect='auto'))

        regions = self.__contrastLabels[stim1, stim2]

        if len(regions) == 0:
            txt = 'Difference in brain activity is\nnot strong enough to localise!'
        else:

            txt = 'Brain activity found in these areas:\n\n'
            for r in regions:
                txt = txt + '  ' + r + '\n'

            txt = txt + '\nCross these regions off\nfrom the brain hat!'

        self.__finalPatches.append(self.__axis.text(
            0.8, 1.8, txt, ha='center'))

        self.__canvas.draw()


    def __clearFinalMovieFrame(self):
        for p in self.__finalPatches:
            p.remove()
        self.__finalPatches = None
        self.__canvas.draw()

    def __reset(self, ev):
        self.__clearFinalMovieFrame()
        self.__runButton.Enable()
        self.__resetButton.Disable()
        self.__stimPanel.Enable()


    def run(self, *a, **kwa):

        if len(self.__activeStim) == 0:
            return

        xdelta = 2 / 50.0

        def update(xpos):
            if xpos >= 1:
                self.__endMovie()

                if len(self.__activeStim) == 1:
                    self.__finalMovieFrameOneStimulus()
                else:
                    self.__finalMovieFrameTwoStimuli()
                self.__resetButton.Enable()
            else:
                self.__movieFrame(xpos)
                wx.CallLater(50, update, xpos + xdelta)

        self.__runButton.Disable()
        self.__stimPanel.Disable()
        self.__initMovie()
        wx.CallAfter(update, 0)


    def __stimulusOn(self, ev):
        stim = op.splitext(op.basename(ev.stimulus))[0]
        self.__activeStim.append(stim)
        self.__drawPlot()


    def __stimulusOff(self, ev):
        stim = op.splitext(op.basename(ev.stimulus))[0]
        self.__activeStim.remove(stim)
        self.__drawPlot()


    def __drawPlot(self, *a, **kwa):

        self.__axis.clear()

        cwidth, cheight = map(float, self.__canvas.get_width_height())
        stim            = self.__activeStim

        bmps   = [self.__stimimgs[  s] for s in stim]
        levels = [self.__stimlevels[s] for s in stim]
        levels = np.array(levels)

        xdata = np.linspace(0, 1, 50 * len(stim))
        ydata = np.ones(len(xdata))

        for idx, level in enumerate(levels):
            ydata[idx * 50:idx * 50 + 50] *= level
            ydata[idx * 50:idx * 50 + 50] += (0.5 - np.random.random(50)) * 0.1

        for i, (bmp, level) in enumerate(zip(bmps, levels)):

            # data limits are hard coded
            # in calls to set_x/ylim below
            xlen    = 0.1
            xcentre = (i + 0.5) / len(stim)
            ycentre = level

            extent =  calc_extent(
                bmp, xlen, xcentre, ycentre, 1.2, 5.5, self.__canvas)

            self.__axis.imshow(bmp,
                               extent=extent,
                               interpolation='bilinear',
                               aspect='auto',
                               zorder=2)

        self.__axis.plot(xdata, ydata, lw=3, c='r', zorder=1)

        self.__axis.set_xlim(-0.1, 1.1)
        self.__axis.set_ylim(-0.5, 5)
        self.__axis.set_xticks([0, 1])
        self.__axis.set_xticklabels(['Start', 'Finish'])
        self.__axis.set_yticks([])

        self.__axis.plot([0, 1], [-0.5, -0.5], c='k', lw=2, zorder=0)
        self.__axis.tick_params(axis='y', which='both', length=0)
        self.__axis.tick_params(axis='x',
                                which='both',
                                length=0,
                                direction='in',
                                pad=-20)
        slabel, elabel = self.__axis.xaxis.get_ticklabels()
        slabel.set_horizontalalignment('left')
        elabel.set_horizontalalignment('right')

        self.__axis.set_xlabel('Time', va='bottom')
        self.__axis.xaxis.set_label_coords(0.5, 10.0 / cheight)

        self.__axis.set_ylabel('Brain activity', ha='center')
        self.__axis.yaxis.set_label_coords(40.0 / cwidth, 0.25)

        self.__canvas.draw()

class StimSelectDialog(wx.Dialog):

    def __init__(self, parent, options, correct):
        wx.Dialog.__init__(self, parent,
                           title='Select experimental stimuli')

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)

        self.options = options
        self.correct = correct

        self.label = wx.StaticText(
            self, label='Select the experimental stimuli you wish to use')

        self.checkboxes = {}

        self.sizer.Add(self.label, flag=wx.EXPAND | wx.ALL, border=5)

        for option in options:
            cbox = wx.CheckBox(self, label=option)
            self.sizer.Add(cbox, flag=wx.EXPAND | wx.ALL, border=5)
            self.checkboxes[option] = cbox
            cbox.Bind(wx.EVT_CHECKBOX, self.__onCheckbox)

        self.ok = wx.Button(self, label='Ok')
        self.ok.Disable()
        self.ok.Bind(wx.EVT_BUTTON, self.__onButton)

        self.sizer.Add(self.ok, flag=wx.EXPAND | wx.ALL, border=5)

        self.Fit()


    def __onCheckbox(self, ev):
        finished = True
        for option, cbox in self.checkboxes.items():
            finished = finished and (cbox.GetValue() == (option in self.correct))
        self.ok.Enable(finished)

    def __onButton(self, ev):
        self.EndModal(wx.ID_OK)
