#!/usr/bin/env python


import os.path as op

import wx
import wx.media as wxm

from . import lockdlg


class HelpPanel(wx.Panel):

    def __init__(self,
                 parent,
                 dataDir,
                 videoFile,
                 autoplay=True,
                 lockcode=None,
                 lockmsg=None,
                 locktitle=None,
                 lockfirst=True):
        wx.Panel.__init__(self, parent)

        self.__dataDir   = dataDir
        self.__videoFile = videoFile
        self.__helpBtn   = wx.Button(self, label='Help')
        self.__vidSizer  = wx.BoxSizer(wx.VERTICAL)
        self.__mainSizer = wx.BoxSizer(wx.VERTICAL)

        self.__vidSizer.Add(self.__helpBtn,   flag=wx.EXPAND)
        self.__vidSizer.Add(self.__mainSizer, flag=wx.EXPAND, proportion=1)

        self.__helpBtn.Bind(wx.EVT_BUTTON, self.__playHelp)

        self.SetSizer(self.__vidSizer)

        self.__autoplay  = autoplay
        self.__lockcode  = lockcode
        self.__locktitle = locktitle
        self.__lockmsg   = lockmsg
        self.__lockfirst = lockfirst


    def __playHelp(self, ev):
        vfile = op.join(self.__dataDir, self.__videoFile)
        vdlg = VideoDialog(self, vfile)
        vdlg.CentreOnParent()
        vdlg.ShowModal()


    def onshow(self):

        autoplay  = self.__autoplay
        lockcode  = self.__lockcode
        locktitle = self.__locktitle
        lockmsg   = self.__lockmsg
        lockfirst = self.__lockfirst

        if autoplay and not lockfirst:
            self.__playHelp(None)

        if lockcode is not None:
            frame = self.GetTopLevelParent()
            lock = lockdlg.LockDialog(frame,
                                      lockcode,
                                      title=locktitle,
                                      msg=lockmsg,
                                      freeze=frame)

            lock.CentreOnParent()
            lock.ShowModal()

        if autoplay and lockfirst:
            self.__playHelp(None)



    @property
    def mainSizer(self):
        return self.__mainSizer


class VideoDialog(wx.Dialog):
    def __init__(self, parent, videoFile):

        w,h = parent.GetTopLevelParent().GetSize().Get()

        w = int(round(0.9 * w))
        h = int(round(0.9 * h))

        wx.Dialog.__init__(self,
                           parent,
                           size=(w,h),
                           style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.__media = wxm.MediaCtrl(self)
        self.__videoFile = videoFile

        self.__media.Bind(wxm.EVT_MEDIA_LOADED,   self.__onLoad)
        self.__media.Bind(wxm.EVT_MEDIA_FINISHED, self.__onFinish)


    def ShowModal(self):
        self.__media.Load(self.__videoFile)
        wx.Dialog.ShowModal(self)


    def __onLoad(self, ev):
        self.__media.Play()


    def __onFinish(self, ev):
        self.EndModal(wx.ID_OK)
