#!/usr/bin/env python
#
# main.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import sys
import os.path as op

import wx
import wx.adv

import fsleyes_widgets.notebook as nb

import                           fsleyes
import fsleyes.gl             as fslgl
import fsleyes.main           as fslmain
import fsleyes.overlay        as fsloverlay
import fsleyes.colourmaps     as fslcm
import fsleyes.displaycontext as fsldc

from . import                  lockdlg
from . import                  regpanel
from . import contrastpanel as cpanel
from . import                  fmripanel
from . import                  surgerypanel


class MainFrame(wx.Frame):

    def __init__(self, dataDir, overlayList, displayCtx, sequential=True):
        wx.Frame.__init__(self, None, title='SHElock Holmes')

        self.__sequential     = sequential
        self.overlayList      = overlayList
        self.displayCtx       = displayCtx
        self.__mainsizer      = wx.BoxSizer(wx.HORIZONTAL)

        with open(op.join(dataDir, 'registration', 'code.txt')) as f:
            regcode = f.read().strip()
        with open(op.join(dataDir, 'fmri', 'code.txt')) as f:
            fmricode = f.read().strip()

        self.__regpanel = regpanel.RegistrationPanel(
            self,
            dataDir,
            1,
            lockcode=regcode,
            lockmsg='Enter your password to begin',
            locktitle='Password required')

        self.__contrastpanel = cpanel.ContrastPanel(
            self, dataDir, 2)

        self.__fmripanel = fmripanel.FMRIPanel(
            self,
            dataDir,
            3,
            lockcode=fmricode,
            lockmsg='Enter the code from the hobby box to continue',
            locktitle='Code required',
            lockfirst=False)

        self.__surgerypanel = surgerypanel.SurgeryPanel(
            self, dataDir, 4)

        self.__notebook = nb.Notebook(self, (wx.LEFT       |
                                             wx.HORIZONTAL |
                                             wx.SUNKEN_BORDER))

        self.__notebook.AddPage(self.__regpanel,       '1. Registration')
        self.__notebook.AddPage(self.__contrastpanel,  '2. Contrasts')
        self.__notebook.AddPage(self.__fmripanel,      '3. FMRI')
        self.__notebook.AddPage(self.__surgerypanel,   '4. Surgery')

        self.__notebook.ShowPage(0)
        if self.__sequential:
            self.__notebook.DisablePage(1)
            self.__notebook.DisablePage(2)
            self.__notebook.DisablePage(3)

        self.__notebook.SetButtonColours(disabledText='#d0d0d0')

        self.__mainsizer.Add(self.__notebook, flag=wx.EXPAND, proportion=1)

        self.SetSizer(self.__mainsizer)

        wx.CallAfter(self.__regpanel.onshow)

    def completed(self, stage):
        if self.__sequential:

            if stage < 4:
                self.__notebook.EnablePage( stage)
                self.__notebook.ShowPage(   stage)
                self.__notebook.SetSelection(self.__notebook.GetSelection())
                self.__notebook.DisablePage(stage - 1)

                if   stage == 1: self.__contrastpanel.onshow()
                elif stage == 2: self.__fmripanel.onshow()
                elif stage == 3: self.__surgerypanel.onshow()
            else:
                self.Disable()


def main():

    if getattr(sys, 'frozen', False):
        dataDir = op.abspath(
            op.join(op.dirname(__file__), '..', '..', '..', '..', 'Resources', 'resources'))
    else:
        dataDir = op.abspath(
            op.join(op.dirname(__file__), '..', 'resources'))


    noseq         = 'noseq' in sys.argv
    nofullscreen = 'nofullscreen' in sys.argv
    overlayList   = fsloverlay.OverlayList()
    masterDC      = fsldc.DisplayContext(overlayList)
    displayCtx    = fsldc.DisplayContext(overlayList, parent=masterDC)
    app           = wx.App()

    app.SetAppName('SHElock Holmes')

    icon = wx.adv.TaskBarIcon(iconType=wx.adv.TBI_DOCK)
    icon.SetIcon(wx.Icon(op.join(dataDir, 'icon.png')))

    def init():
        fsleyes.initialise()
        fslcm.init()
        fslgl.getGLContext(parent=frame, ready=fslgl.bootstrap)

    wx.CallAfter(init)

    frame = MainFrame(dataDir, overlayList, displayCtx, not noseq)

    frame.SetSize((1280, 800))
    frame.Centre()
    frame.Show()

    if not nofullscreen:
        wx.CallLater(1000, lambda : frame.ShowFullScreen(True))

    menubar = wx.MenuBar()
    frame.SetMenuBar(menubar)
    macmenu = menubar.OSXGetAppleMenu()
    macmenu.Append(wx.ID_EXIT, 'Quit\tCtrl-Q')


    app.MainLoop()


if __name__ == '__main__':
    main()
