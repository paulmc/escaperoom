#!/usr/bin/env python
#
# Registration stage


import os.path as op

import wx
import numpy as np


import fsleyes_widgets.floatspin  as floatspin

import fsl.data.image as fslimage
import fsleyes_widgets.utils.progress as progress
import fsleyes_widgets.floatspin as floatspin
import fsleyes.views.orthopanel as ortho


from . import helppanel


class RegistrationPanel(helppanel.HelpPanel):

    def __init__(self, mainFrame, dataDir, stage, **kwargs):

        helppanel.HelpPanel.__init__(self, mainFrame, dataDir, 'registration.mp4',
                                     **kwargs)

        self.__mainFrame = mainFrame
        self.__dataDir   = dataDir
        self.__stage     = stage

        fsargs = {
            'minValue'  : -90,
            'maxValue'  : 90,
            'increment' : 1,
            'value'     : 0,
            'style'     : floatspin.FSC_INTEGER
        }

        self.__title  = wx.StaticText(
            self, label='Enter the registration parameters')

        self.__xlabel = wx.StaticText(self, label='Scan 1 rotation')
        self.__ylabel = wx.StaticText(self, label='Scan 2 rotation')
        self.__zlabel = wx.StaticText(self, label='Scan 3 rotation')

        self.__xinput = floatspin.FloatSpinCtrl(self, **fsargs)
        self.__yinput = floatspin.FloatSpinCtrl(self, **fsargs)
        self.__zinput = floatspin.FloatSpinCtrl(self, **fsargs)

        self.__submit = wx.Button(self, label='Submit')

        self.__sizer = wx.FlexGridSizer(3, 2, 1, 1)
        self.__sizer.AddGrowableCol(1)

        self.mainSizer.Add((1, 20))
        self.mainSizer.Add(self.__title, flag=wx.EXPAND)
        self.mainSizer.Add((1, 20))
        self.mainSizer.Add(self.__sizer, flag=wx.EXPAND, proportion=1)

        self.__sizer.Add(self.__xlabel, flag=wx.EXPAND)
        self.__sizer.Add(self.__xinput, flag=wx.EXPAND)
        self.__sizer.Add(self.__ylabel, flag=wx.EXPAND)
        self.__sizer.Add(self.__yinput, flag=wx.EXPAND)
        self.__sizer.Add(self.__zlabel, flag=wx.EXPAND)
        self.__sizer.Add(self.__zinput, flag=wx.EXPAND)

        self.mainSizer.Add((1, 20))
        self.mainSizer.Add(self.__submit, flag=wx.EXPAND)
        self.mainSizer.Add((1, 20))

        self.__submit.Bind(wx.EVT_BUTTON, self.__onSubmit)


    def __onSubmit(self, ev):
        underlay = op.join(self.__dataDir,
                           'registration',
                           'registration_underlay.nii.gz')
        goodimg = op.join(self.__dataDir,
                          'registration',
                          'good_registration.nii.gz')
        badimg  = op.join(self.__dataDir,
                          'registration',
                          'bad_registration.nii.gz')
        angles  = np.loadtxt(op.join(self.__dataDir,
                                     'registration',
                                     'correct_angles.txt'))

        angles, tol = angles[:3], angles[3]

        xval = self.__xinput.GetValue()
        yval = self.__yinput.GetValue()
        zval = self.__zinput.GetValue()
        vals = [xval, yval, zval]

        result = np.all(np.abs(angles - vals) <= tol)

        dlg = SubmitDialog(
            self,
            self.__mainFrame,
            result,
            angles,
            underlay,
            goodimg,
            badimg)
        x, y = self.GetSize().Get()
        x = int(round(0.9 * x))
        y = int(round(0.9 * y))
        dlg.SetSize((x, y))
        dlg.CentreOnParent()
        dlg.ShowModal()

        if result:
            self.__mainFrame.completed(self.__stage)


class SubmitDialog(wx.Dialog):
    def __init__(self,
                 parent,
                 mainFrame,
                 result,
                 angles,
                 underlay,
                 goodimg,
                 badimg):

        wx.Dialog.__init__(self, parent, style=wx.DEFAULT_DIALOG_STYLE |
                                               wx.RESIZE_BORDER)

        passmsg1 = 'Well done, you have successfully registered the brain!'
        failmsg1 = 'Sorry, this registration is not good enough - ' \
                   'keep trying!.'
        passmsg2 = 'The exact registration angles were:'

        if result: msg1 = passmsg1
        else:      msg1 = failmsg1
        if result: img  = fslimage.Image(goodimg)
        else:      img  = fslimage.Image(badimg)
        if result: msg2 = passmsg2
        else:      msg2 = None

        underlay = fslimage.Image(underlay)

        mainFrame.overlayList[:] = []
        mainFrame.overlayList.append(underlay)
        mainFrame.overlayList.append(img, overlayType='mask')
        opts = mainFrame.displayCtx.getOpts(img)
        opts.interpolation = 'spline'
        opts.outline       = True
        opts.colour        = (0, 255, 0)

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)

        msg1 = wx.StaticText(self, label=msg1)
        sizer.Add(msg1, flag=wx.EXPAND)

        self.__ortho = ortho.OrthoPanel(
            self,
            mainFrame.overlayList,
            mainFrame.displayCtx,
            mainFrame)
        self.__ortho.sceneOpts.showLabels = False

        sizer.Add(self.__ortho, flag=wx.EXPAND, proportion=1)

        if result:
            msg2 = wx.StaticText(self, label=msg2)
            sizer.Add(msg2, flag=wx.EXPAND)

            angles = 'x={} y={} z={}'.format(*angles)
            angles = wx.StaticText(self, label=angles)
            afont  = wx.Font(wx.FontInfo()
                             .FaceName('Courier')
                             .Bold())
            afont.SetPointSize(18)

            angles.SetFont(afont)

            sizer.Add(angles, flag=wx.EXPAND, proportion=0.5)


        button = wx.Button(self, label='Ok')
        sizer.Add(button, flag=wx.EXPAND)

        button.Bind(wx.EVT_BUTTON, self.__onOk)

    def __onOk(self, ev):
        self.__ortho.destroy()
        self.EndModal(wx.ID_OK)
