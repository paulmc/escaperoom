#!/usr/bin/env python
#
# stimpanel.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os.path as op
import            glob

import numpy                        as np
import                                 wx
import wx.lib.newevent              as wxevent
import fsleyes_widgets.bitmaptoggle as bt


StimOnEvent,  EVT_STIM_ON  = wxevent.NewEvent()
StimOffEvent, EVT_STIM_OFF = wxevent.NewEvent()


class StimulusPanel(wx.Panel):
    def __init__(self, parent, datadir, maxon=2):
        wx.Panel.__init__(self, parent)

        stimfiles = sorted(glob.glob(op.join(datadir, 'fmri', 'stimuli', '*.png')))
        nstim     = len(stimfiles)
        bmps      = [wx.Bitmap(img, wx.BITMAP_TYPE_PNG) for img in stimfiles]
        sizes     = [b.GetSize().Get() for b in bmps]
        ratios    = [w / h for w, h in sizes]
        bmps      = [b.ConvertToImage() for b in bmps]
        bmps      = [b.Rescale(100, 100 / r) for b, r in zip(bmps, ratios)]
        bmps      = [b.ConvertToBitmap() for b in bmps]

        self.__maxon = maxon
        self.__sizer = wx.GridSizer(np.ceil(nstim / 2.0), 2, 5, 5)

        self.__sizer.Add((1, 1), flag=wx.EXPAND)

        btns = [bt.BitmapToggleButton(self, trueBmp=b) for b in bmps]

        self.buttons = {}
        self.enabled = {}
        self.__globalEnable = True

        for b, f in zip(btns, stimfiles):
            f = op.splitext(op.basename(f))[0]
            self.buttons[f] = b
            self.__sizer.Add(b, flag=wx.EXPAND)
            b.stimFile = f
            b.Bind(bt.EVT_BITMAP_TOGGLE, self.__onButton)
            self.enabled[f] = True

        self.SetSizer(self.__sizer)


    def Disable(self):
        self.Enable(False)


    def Enable(self, enable=True):
        self.__globalEnable = enable
        self.__updateEnabledState()


    def EnableButton(self, stimid, enable=True):
        self.enabled[stimid] = enable
        self.__updateEnabledState()


    def __updateEnabledState(self):

        numon = sum([b.GetValue() for b in self.buttons.values()])

        for sid, b in self.buttons.items():
            enable = (self.__globalEnable and
                      self.enabled[sid]   and
                      ((numon < self.__maxon) or b.GetValue()))
            b.Enable(enable)


    def __onButton(self, ev):

        btn = ev.GetEventObject()

        if ev.value: evtype = StimOnEvent
        else:        evtype = StimOffEvent

        self.__updateEnabledState()

        wx.PostEvent(self, evtype(stimulus=btn.stimFile))
