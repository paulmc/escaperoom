#!/usr/bin/env python
#
# surgerypanel.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os.path as op

import wx


from . import helppanel


class SurgeryPanel(helppanel.HelpPanel):

    def __init__(self, mainFrame, dataDir, stage, **kwargs):
        helppanel.HelpPanel.__init__(self, mainFrame, dataDir, 'surgery.mp4',
                                     **kwargs)
        self.__mainFrame = mainFrame
        self.__dataDir   = dataDir
        self.__stage     = stage


    def onshow(self):
        helppanel.HelpPanel.onshow(self)
        with open(op.join(self.__dataDir, 'surgery', 'regions.txt')) as f:
            lines   = f.read().strip().split('\n')
            lines   = [l.split(',') for l in lines]
            regions = [l[0].strip() for l in lines]
            correct = [l[0].strip() for l in lines
                       if l[1].strip().lower() == 'true']
        dlg = IncisionDialog(self, regions, correct)
        dlg.CentreOnParent()
        dlg.ShowModal()
        self.showcode()


    def showcode(self):

        with open(op.join(self.__dataDir, 'surgery', 'code.txt')) as f:
            code = f.read().strip()

        sizer = wx.BoxSizer(wx.VERTICAL)
        msg   = wx.StaticText(
            self,
            label='You are ready to perform surgery! Use this code '
                  'to access the surgery theatre:')
        label = wx.StaticText(self, label=code)
        afont  = wx.Font(wx.FontInfo() .FaceName('Courier').Bold())
        afont.SetPointSize(18)
        label.SetFont(afont)

        self.SetSizer(sizer)
        sizer.Add((1, 1), flag=wx.EXPAND, proportion=1)
        sizer.Add(msg, flag=wx.ALIGN_CENTRE)
        sizer.Add((20, 20))
        sizer.Add(label, flag=wx.ALIGN_CENTRE)
        sizer.Add((1, 1), flag=wx.EXPAND, proportion=1)

        self.Layout()



class IncisionDialog(wx.Dialog):
    def __init__(self, parent, regions, correct, freeze=None):

        style = wx.DEFAULT_DIALOG_STYLE & ~wx.CLOSE_BOX

        wx.Dialog.__init__(
            self, parent,
            title='Select incision point',
            style=style)

        self.__regions = regions
        self.__correct = correct

        self.__msg    = wx.StaticText(
            self, label='Select the surgical incision point')
        self.__choice = wx.Choice(self, choices=regions)
        self.__ok     = wx.Button(self, label='Ok')

        self.__ok.Bind(wx.EVT_BUTTON, self.__onOk)

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(sizer)

        sizer.Add(self.__msg,    flag=wx.EXPAND | wx.ALL, border=5)
        sizer.Add(self.__choice, flag=wx.EXPAND | wx.ALL, border=5)
        sizer.Add(self.__ok,     flag=wx.EXPAND | wx.ALL, border=5)

        self.Fit()


    def __onOk(self, ev):

        sel = self.__choice.GetSelection()

        if sel < 0 or sel >= len(self.__regions):
            return

        if self.__regions[sel] in self.__correct:
            self.EndModal(wx.ID_OK)
