#!/usr/bin/env python
#
# update_fmri_resources.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os
import os.path as op
import glob
import shutil


stim_mappings = [
    ('baseline',   'no activity'),
    ('drums',      'random activity'),
    ('guitar',     'random activity'),
    ('piano',      'auditory'),
    ('saxophone',  'random activity'),
    ('football',   'random activity'),
    ('golf',       'random activity'),
    ('swimming',   'random activity'),
    ('tennis',     'motor'),
    ('book',       'random activity'),
    ('cinema',     'random activity'),
    ('tv',         'random activity'),
    ('youtube',    'visual')]

def main():

    resdir    = op.dirname(__file__)
    stimdir   = op.join(resdir, 'fmri', 'stimuli')
    actdir    = op.join(resdir, 'fmri', 'activations')
    condir    = op.join(resdir, 'fmri', 'contrasts')
    stimfiles = list(glob.glob(op.join(resdir, 'raw_stimuli',     '*.png')))
    actfiles  = list(glob.glob(op.join(resdir, 'raw_activations', '*.png')))
    noact     = [f for f in actfiles if 'no activity' in f.lower()][0]

    print('stimfiles')
    print(stimfiles)
    print('actfiles')
    print(actfiles)

    for i, (si, ai) in enumerate(stim_mappings):

        print('  ', i, si, ai)
        sif = [f for f in stimfiles if si.lower() in f.lower()][0]
        aif = [f for f in actfiles  if ai.lower() in f.lower()][0]

        shutil.copy(sif, op.join(stimdir, '{:02d}.png'.format(i + 1)))
        shutil.copy(aif, op.join(actdir,  '{:02d}.png'.format(i + 1)))

        for j in range(i + 1, len(stim_mappings)):

            sj, aj = stim_mappings[j]
            ajf    = [f for f in actfiles if aj.lower() in f.lower()][0]

            if si == 'baseline': cfile = ajf
            else:                cfile = noact

            shutil.copy(
                cfile,
                op.join(condir, '{:02d}_{:02d}.png'.format(i + 1, j + 1)))

if __name__ == '__main__':
    main()
