#!/usr/bin/env python


import os.path as op
import os
import shutil
import glob
import pkgutil
import itertools as it

from collections import defaultdict

from setuptools import setup, find_packages

from py2app.build_app import py2app


basedir = op.abspath(op.dirname(__file__))

def list_all_files(in_dir):
    """List all files ``in_dir``. """
    for dirname, dirs, files in os.walk(in_dir):
        for filename in files:
            yield op.join(dirname, filename)


def build_asset_list():

    fsleyesdir   = package_path('fsleyes')
    allfiles     = list_all_files(op.join(basedir, 'resources'))
    fsleyesfiles = list_all_files(op.join(fsleyesdir, 'fsleyes', 'assets'))
    flist        = defaultdict(list)

    for f in allfiles:
        dirname = op.relpath(op.dirname(f), basedir)
        flist[dirname].append(f)

    for f in fsleyesfiles:
        dirname = op.relpath(op.dirname(f), op.join(fsleyesdir, 'fsleyes'))
        flist[dirname].append(f)

    return list(flist.items())


def package_path(pkg, in_=False):
    """Returns the directory path to the given python package. """
    fname   = pkgutil.get_loader(pkg).get_filename()
    dirname = op.dirname(fname)

    if in_: return op.abspath(dirname)
    else:   return op.abspath(op.join(dirname, '..'))


class standalone(py2app):
    description = 'Builds a standalone OSX application using py2app'

    def finalize_options(self):

        entrypt = op.join(basedir, 'entrypt.py')
        plist   = op.join(basedir, 'Info.plist')
        assets  = build_asset_list()

        self.verbose             = False
        self.quiet               = True
        self.argv_emulation      = True
        self.no_chdir            = True
        self.optimize            = True
        self.app                 = [entrypt]
        self.plist               = plist
        self.resources           = assets
        self.matplotlib_backends = ['wx_agg']

        py2app.finalize_options(self)


    def run(self):

        py2app.run(self)

        distdir     = op.join(basedir, 'dist')
        contentsdir = op.join(distdir, 'SHElock Holmes.app', 'Contents')
        dylibs      = []

        # py2app seems to get the wrong
        # version of libpng, which causes
        # segfaults
        pildir = package_path('matplotlib')
        dylibs.append(
            op.join(pildir, 'matplotlib', '.dylibs', 'libpng16.16.dylib'))

        for dylib in dylibs:
            if op.exists(dylib):
                dest = op.join(contentsdir, 'Frameworks', op.basename(dylib))
                shutil.copy(dylib, dest)
                os.chmod(dest, 0o644)


if __name__ == '__main__':
    setup(
        name='SHElock Holmes',
        version='0.1.0',
        packages=find_packages(),
        cmdclass={'standalone' : standalone})
